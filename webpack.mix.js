let mix = require('laravel-mix');

mix.options({ processCssUrls: false });

mix.webpackConfig({
    resolve: {
        extensions: ['.scss'],
        alias: {
            'styles':  path.join(__dirname, 'themes/observatorio/assets/scss') 
        },
    },
})

mix.sass('./themes/observatorio/assets/scss/app.scss', 'themes/observatorio/dist/app.css');
mix.js('./themes/observatorio/assets/javascript/app.js', 'themes/observatorio/dist');
