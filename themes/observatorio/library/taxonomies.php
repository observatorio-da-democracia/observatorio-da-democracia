<?php
$tax_videos = new Taxonomy('category_videos', ['videos'], new Label('Categoria de Vídeo','Categorias de Vídeos'));
$tax_editorias = new Taxonomy('editorias', ['post', 'videos', 'articles', 'documents'], new Label('Editoria','Editorias'));
$tax_featured = new Taxonomy('is_featured', ['post', 'videos', 'articles', 'documents'], new Label('Destque','Destaques'));

$tax_register = new Taxonomies();
$tax_register->add($tax_videos, $tax_editorias, $tax_featured)->hook();