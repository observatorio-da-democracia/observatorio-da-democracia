<?php
add_action('cmb2_admin_init', function () {
    
    // PAGE METABOXES
    $cmb = new_cmb2_box(array(
        'id' => 'page_metabox',
        'title' => "Configurações da Página",
        'object_types' => array('page'), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true,
    ));

    $terms_editorias = get_terms([
        'taxonomy' => 'editorias',
        'hide_empty' => false,
        'parent' => '0'
    ]);

    $cmb->add_field(array(
        'name' => 'Esta página é uma subhome de Editorias?',
        'desc' => '',
        'id' => 'subhome',
        'type' => 'radio',
        'options' => array_flatten(array_map(function ($x) {
            return [ 'subhome_' . $x->term_id => $x->name];
        }, $terms_editorias))
    ));

});
