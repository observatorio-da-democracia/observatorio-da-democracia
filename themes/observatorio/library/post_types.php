<?php
$cpt_articles = new CPT('articles', new Label('Artigo','Artigos'));

$cpt_documents = new CPT('documents', new Label('Documento','Documentos'));

$cpt_videos = new CPT('videos', new Label('Video','Videos'));
unset_by_value($cpt_videos->args['taxonomies'], 'category');

$cpt_layout_parts = new CPT('layout_parts', new Label('Partes do Layout','Parte de Layout'));


$cpt_register = new CPTs();
$cpt_register->add($cpt_articles, $cpt_videos, $cpt_documents, $cpt_layout_parts)->hook();

//Rename Posts menu item to Notícias
function observatorio_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Notícias';
    $submenu['edit.php'][5][0] = 'Notícias';
    $submenu['edit.php'][10][0] = 'Adicionar Notícias';
    $submenu['edit.php'][16][0] = 'Tags de Notícias';
}
function observatorio_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Notícias';
    $labels->singular_name = 'Notícias';
    $labels->add_new = 'Adicionar Notícias';
    $labels->add_new_item = 'Adicionar Notícias';
    $labels->edit_item = 'Edit Notícias';
    $labels->new_item = 'Notícias';
    $labels->view_item = 'Ver Notícias';
    $labels->search_items = 'Buscar por Notícias';
    $labels->not_found = 'No Notícias found';
    $labels->not_found_in_trash = 'No Notícias found in Trash';
    $labels->all_items = 'Todas as Notícias';
    $labels->menu_name = 'Notícias';
    $labels->name_admin_bar = 'Notícias';
}
 
add_action( 'admin_menu', 'observatorio_change_post_label' );
add_action( 'init', 'observatorio_change_post_object' );