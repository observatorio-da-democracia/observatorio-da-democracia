<?php

$api_key = $instance['youtube_api_key'];
$list_id = $instance['youtube_list_id'];

$num = 4;
if($instance['youtube_list'] == 'playlist'){
    $url = "https://www.googleapis.com/youtube/v3/playlistItems?key={$api_key}&playlistId={$list_id}&maxResults={$num}&part=snippet,id&order=date";
} else if($instance['youtube_list'] == 'channel'){
    $url = "https://www.googleapis.com/youtube/v3/search?key={$api_key}&channelId={$list_id}&maxResults={$num}&part=snippet,id&order=date";
}

$cache_key = __FILE__ . ':' . $url;

if(apcu_exists($cache_key)){
    $result = apcu_fetch($cache_key);
} else {
    $result = json_decode(file_get_contents($url));
    apcu_store($cache_key, $result, 300);
}

if (!is_object($result) || count($result->items) === 0) {
    $result = (object) ['items' => []];
}


$extract_vid = function($video){
    if(isset($video->snippet->resourceId->videoId)){
        return $video->snippet->resourceId->videoId;
    } else if (isset($video->id->videoId)){
        return $video->id->videoId;
    }
};
?>

<div class="featured-videos">
    <div class="row">
        <div class="column large-5 small-12">
            <h3 class="featured-videos--title">Vídeos</h3>
            <div class="featured-videos--description"><?= $instance['text'] ?></div>
            <?php if($instance['button_href']): ?>
                <div class="mb-20">
                    <a href="<?= $instance['button_href'] ?>" class="button red" target="<?= $instance['button_external_link'] ? '_blank' : '_self' ?>">Ver mais vídeos</a>
                </div>
            <?php endif; ?>
            <div class="button white composed">
                <span>inscreva-se</span>
                <div class="g-ytsubscribe" data-channelid="UCXQR-5Obb1uAQthcWP3ejFA" data-layout="default" data-count="default"></div>
            </div>
        </div>
        <div class="column large-7 small-12 mt-sm-20">
            <div class="row">
                <iframe class="column small-12 large-12 js-featured-videos--iframe p-0" src="https://www.youtube.com/embed/<?= $extract_vid($result->items[0]) ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="row">
                <?php foreach($result->items as $index => $video): ?>
                    <div class="column small-3 large-3  <?= $video->snippet->liveBroadcastContent == 'live' ? 'live' : '' ?>">
                        <a href="#" class="featured-videos--thumbnail js-featured-videos--thumbnail <?= $index == 0 ? 'active' : '' ?>" data-video-id="<?= $extract_vid($video) ?>" title="<?= htmlentities($video->snippet->title) ?>">
                            <img width="100%" src="<?= $video->snippet->thumbnails->medium->url ?>" >
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>