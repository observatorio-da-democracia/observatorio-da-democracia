<?php

/*
  Widget Name: Vídeos em Destaque
  Description: Adiciona uma seção com vídeos de um canal ou playlist do YouTube
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class FeaturedVideos extends \SiteOrigin_Widget {

    function __construct() {
        wp_enqueue_script( 'featured-videos', get_stylesheet_directory_uri() . '/library/pagebuilder-widgets/featured-videos/featured-videos.js', array( 'jquery' ) );

        $fields = [
            'text' => [
                'type' => 'text',
                'label' => 'Texto da seção',
                'default' => "Siga nosso canal no YouTube e fique por dentro dos nossos conteúdos de vídeos"
            ],
            'button_href' => [
                'type' => 'link',
                'sanitize' => 'url',
                'label' => 'Link do botão "ver mais vídeos"'
            ],
            'button_external_link' => [
                'type' => 'checkbox',
                'label' => 'Link externo',
                'description' => 'O link do botão deve abrir numa nova aba',
                'default' => FALSE,
            ],
            'youtube_api_key' => [
                'type' => 'text',
                'label' => 'Chave da API do youtube'
            ],
            'youtube_list' => [
                'type' => 'select',
                'label' => 'Tipo de lista do youtube',
                'default' => 'channel',
                'options' => [
                    'channel' => 'Canal',
                    'playlist' => 'Playlist'
                ]
            ],
            'youtube_list_id' => [
                'type' => 'text',
                'label' => 'Id da lista (canal ou playlist)'
            ],
        ];

        parent::__construct('featured-videos', 'Vídeos em Destaque', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Adiciona uma seção com vídeos de um canal ou playlist do YouTube'
                ], [], $fields, plugin_dir_path(__FILE__)
        );

        wp_enqueue_script( 'youtube-plataform', 'https://apis.google.com/js/platform.js' );
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('featured-videos', __FILE__, 'widgets\FeaturedVideos');
