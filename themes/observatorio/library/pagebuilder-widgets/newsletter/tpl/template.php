<div class="newsletter component-wrapper">
    <div class="row">
        <div class="column large-12 small-12">
            <h3 class="newsletter--title"><?php echo $instance['title'] ?></h3>
            <p class="newsletter--description"><?php echo $instance['description'] ?></p>
        </div>
    </div>
    <div class="row newsletter--form">
        <div class="column large-5">
            <input type="text" placeholder="seu e-mail" class="input-outline">
        </div>
        <div class="column large-5">
            <input type="text" placeholder="seu telefone" class="input-outline">
        </div>
        <div class="column large-2">
            <a href="#" class="button white block"><?php echo $instance['button_text'] ?></a>
        </div>
    </div>

    <div class="newsletter-success hide">
        <?php echo $instance['success_message'] ?>
    </div>
</div>