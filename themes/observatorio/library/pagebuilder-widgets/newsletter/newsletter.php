<?php

/*
  Widget Name: Newsletter
  Description: Formulário para assinar a Newsletter
  Author: hacklab/
  Author URI: https://hacklab.com.br
 */

namespace widgets;

class Newsletter extends \SiteOrigin_Widget {
    function __construct() {
        
        $fields = [
            'title' => [
                'type' => 'text',
                'label' => 'Título',
                'default' => 'Assine o Boletim'
            ],
            'description' => [
                'type' => 'text',
                'label' => 'Descrição',
                'default' => 'Cadastre-se e fique por dentro das notícias e conteúdos'
            ],
            'button_text' => [
                'type' => 'text',
                'label' => 'Texto do Botão',
                'default' => 'Assinar!'
            ],
            'success_message' => [
                'type' => 'text',
                'label' => 'Mensagem de agradecimento',
                'default' => 'Obrigado por assinar nosso boletim'
            ],
        ];

        parent::__construct('newsletter', 'Newsletter', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Formulário de assinatura da newsletter'
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('newsletter', __FILE__, 'widgets\Newsletter');
