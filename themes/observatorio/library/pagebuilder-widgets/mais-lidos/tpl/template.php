<?php
$card_query_posts = new WP_Query( $query_args );
?>
<?php if ($card_query_posts->have_posts()): ?>
  <div class="most-read">
    <div class="most-read--title">Mais Lidas</div>
    <ul class="most-read--list">
        <?php while($card_query_posts->have_posts()): $card_query_posts->the_post();?>
            <li class="most-read--list-item">
                <a href="<?= get_the_permalink() ?>">
                    <div class="most-read--list-title"><?php the_title() ?></div>
                    <span class="most-read--list-category"><?php the_channel(get_post_type()) ?> |
                    <span class="most-read--list-author">por <?php the_author() ?></span>
                </a>
            </li>
        <?php endwhile; wp_reset_postdata(); ?>
    </ul>
</div>

<?php endif; ?>

