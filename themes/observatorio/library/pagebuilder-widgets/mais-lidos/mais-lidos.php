<?php

/*
  Widget Name: Lista dos mais lidos
  Description: Adiciona uma lista das postagens mais lidas
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class MaisLidos extends \SiteOrigin_Widget {

    function __construct() {
        $fields = [
            'channels' => [
                'type' => 'checkboxes',
                'label' => "Selecione os canais que devem ser incluidos",
                'default' => 'post',
                'options' => [
                    'post' =>  'Artigos',
                    'videos' =>  'Vídeos',
                    'podcasts' => 'Podcasts',
                    'outrasmidias' => 'Outras Mídias',
                    'blog' => 'Blog da Redação'
                ],
            ],
            'num_days' => [
                'type' => 'number',
                'label' => 'Número de dias',
                'description' => 'número de dias que deve contar para a gerar a lista dos mais lidos (14 por padrão)',
                'default' => 14
            ],
            'num_posts' => [
                'type' => 'number',
                'label' => 'Número de posts',
                'description' => 'número de posts que devem ser listados (4 por padrão)',
                'default' => 4
            ],
        ];

        parent::__construct('mais-lidos', "Lista de posts mais lidos", [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Adiciona uma lista dos mais lidos'
                ], [], $fields, plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

    function get_template_variables($instance, $args) {

        if(false){
            $wpdb = new \wpdb;
        }


        $from = date('Y-m-d', strtotime('- ' . $instance['num_days'] . ' days'));

        
        $cpts = $instance['channels'];

        $pageviews_data = \AjaxPageviews::get_top_viewed($instance['num_posts'], [
            'post_type' => $cpts,
            'from' => $from
        ]);

        $ids = array_map(function($el){ return $el->post_id; }, $pageviews_data);
        if(!$ids){
            $ids = [-1];
        }
        $query_args = [
            'post_type' => $cpts,
            'post__in' => $ids,
            'orderby' => 'post__in',
            'nopaging' => true
        ];
        
        return ['query_args' => $query_args, 'pageviews_data' => $pageviews_data];
    }

}

Siteorigin_widget_register('mais-lidos', __FILE__, 'widgets\MaisLidos');
