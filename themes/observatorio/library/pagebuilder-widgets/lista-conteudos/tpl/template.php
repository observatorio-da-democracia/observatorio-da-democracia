<div class="row">

<card-section 
    class="mt-30"
    hash="<?= $hash ?>"
    title="<?= $instance['title'] ?>" 
    :has-large-post="<?= $instance['layout'] == 1 ? 'true' : 'false' ?>"
    :has-pagination="<?= $instance['has_pagination'] == 1 ? 'true' : 'false' ?>"
    :columns="<?= $instance['column'] ?>" >
</card-section>

</div>