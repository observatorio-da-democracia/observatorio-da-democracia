<?php

/*
  Widget Name: Lista de Conteúdos
  Description: Adiciona uma lista de conteúdos
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class ListaConteudos extends \SiteOrigin_Widget
{


    function __construct()
    {

        $fields = [
            'title' => [
                'type' => 'text',
                'label' => 'Título',
            ],
            'layout' => [
                'type' => 'checkbox',
                'label' => 'Possui card em destaque',
            ],
            'tabs' => [
                'type' => 'checkbox',
                'label' => 'Exibir filtros',
            ],
            'has_pagination' => [
                'type' => 'checkbox',
                'label' => 'Exibir paginação',
                'default' => '1'
            ],
            'column' => [
                'type' => 'number',
                'label' => 'Quantidade de Cards por Linha',
                'default' => '1'
            ],
            'conteudo' => [
                'type' => 'posts',
                'label' => 'Conteúdo'
            ],
        ];

        parent::__construct('lista-conteudos', "Lista de Conteúdos", [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Adiciona uma lista de conteúdos'
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance)
    {
        return 'template';
    }

    function get_style_name($instance)
    {
        return 'style';
    }

    function get_template_variables($instance, $args)
    {
        $query_args = siteorigin_widget_post_selector_process_query($instance['conteudo']);
        $taxonomy_terms = get_terms('editorias', array('hide_empty' => 0, 'parent' => 0));

        if ($instance['tabs']) {
            if( isset($query_args['tax_query']) ){
                $taxonomy_parent = get_term_by( 'slug', $query_args['tax_query'][0]['terms'], 'editorias')->term_id;
                $taxonomy_terms = get_terms('editorias', array('hide_empty' => 0, 'parent' => $taxonomy_parent));
            }

            $taxonomy_slugs = [];

            foreach ($taxonomy_terms as $tax) {
                $taxonomy_slugs[] = $tax->slug;
            }

            $query_args['paged'] = 1;
            $query_args['tax_query'] = [
                'relation' => 'AND',
                [
                    'taxonomy' => 'editorias',
                    'field' => 'slug',
                    'terms' => $taxonomy_slugs
                ]
            ];
        }

        $hash = md5(json_encode($query_args));
        set_transient($hash, $query_args);

        wp_localize_script('no-js', 'card_section_' . $hash . '_tax', $instance['tabs'] == 1 ? $taxonomy_terms : 'false' );

        return ['hash' => $hash ];
    }

}

Siteorigin_widget_register('lista-conteudos', __FILE__, 'widgets\ListaConteudos');
