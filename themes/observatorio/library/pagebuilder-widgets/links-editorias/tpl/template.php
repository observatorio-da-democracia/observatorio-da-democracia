
<?php 
$links = [];

$editorias = get_terms([
    'taxonomy' => 'editorias',
    'hide_empty' => false,
    'parent' => $instance['editoria']
]);

$editorias = array_flatten(array_map(function($x){
    return [ get_term_link($x->term_id) => $x->name  ];

}, $editorias));
?>

<div class="links-editorias card-section featured">
    <div class="card-section--title">
        <h3><?= $instance['title']?></h3>
    </div>

    <div class="links-editorias--content">
        <p class="links-editorias--description"><?= $instance['description'] ?></p>

        <ul class="links-editorias--list">
            <?php foreach($editorias as $url => $name ): ?>
                <li><a href="<?= $url ?>"><?= $name ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>