<?php

/*
  Widget Name: Lista com Links para Editorias
  Description: Uma lista com links para editorias
  Author: hacklab/
  Author URI: https://hacklab.com.br
 */

namespace widgets;

class LinksEditorias extends \SiteOrigin_Widget {

    function __construct() {
        $editorias = get_terms([
            'taxonomy' => 'editorias',
            'hide_empty' => false,
        ]);

        $editorias = array_map(function($x){
            return [ $x->term_id => 'Editorias filhas de "' . $x->name . '"' ];
        }, $editorias);

        array_unshift($editorias, [ '0' => 'Editorias Principais' ]);
        
        $editorias = array_flatten($editorias);

        $fields = [
            'title' => [
                'type' => 'text',
                'label' => 'Título'
            ],
            'description' => [
                'type' => 'text',
                'label' => 'Descrição'
            ],
            'editoria' => [
                'type' => 'select',
                'label' => 'Listar quais editorias?',
                'options' => $editorias
            ],
        ];

        parent::__construct('links-editorias', 'Lista com Links para Editorias', [
            'panels_groups' => [WIDGETGROUP_BANNERS],
            'description' => 'Uma lista com links para editorias'
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('links-editorias', __FILE__, 'widgets\LinksEditorias');
