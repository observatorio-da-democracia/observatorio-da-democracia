<div class="follow-us">
    <div class="card-section featured">
        <div class="row">
            <div class="column small-12 mb-15 card-section--title">
                <h3>Siga nossas redes</h3>
            </div>
            <div class="column small-12">
                <ul class="social-networks">
                    <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
