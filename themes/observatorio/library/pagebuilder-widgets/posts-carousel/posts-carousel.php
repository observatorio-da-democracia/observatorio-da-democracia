<?php

/*
  Widget Name: Carrossel de Posts
  Description: Carrossel dos últimos posts
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class PostsCarousel extends \SiteOrigin_Widget {

    function __construct() {
        wp_enqueue_script('posts-carousel', get_stylesheet_directory_uri() . '/library/pagebuilder-widgets/posts-carousel/posts-carousel.js', ['flickity']);

        $fields = [
            'posts' => [
                'type' => 'posts',
                'label' => 'Lista de posts',
            ],
        ];

        parent::__construct('posts-carousel', 'Carrossel de Posts', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Carrossel dos últimos posts'
                ], [], $fields, plugin_dir_path(__FILE__)
        );

        wp_enqueue_script( 'youtube-plataform', 'https://apis.google.com/js/platform.js' );
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('posts-carousel', __FILE__, 'widgets\PostsCarousel');
