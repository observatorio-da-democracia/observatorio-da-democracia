jQuery(document).ready(function() {

    jQuery('.posts-flickity').flickity({
        autoPlay: 3000,
        cellSelector: '.carousel-cell',
        draggable: true,
        pageDots: true,
        prevNextButtons: false,
        wrapAround: true
    });
});
