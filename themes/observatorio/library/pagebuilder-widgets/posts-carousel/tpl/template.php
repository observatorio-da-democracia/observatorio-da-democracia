<div class="posts-flickity">
    <?php
    $posts = new WP_Query(siteorigin_widget_post_selector_process_query($instance['posts']));
    $subhome = get_post_meta( get_the_ID(), 'subhome', true );

    while ($posts->have_posts()) {
        $posts->the_post();

        $categories = $subhome != 'false' 
        ? wp_get_post_terms(get_the_ID(), 'editorias', [ 'fields' => 'all', 'parent' => str_replace('subhome_', '', $subhome) ]) 
        :  get_the_terms(get_the_ID(), 'editorias');

        ?>
        <div class="carousel-cell"style="background-image: url(<?= get_the_post_thumbnail_url(null,'large') ?>">
            <a class="carousel-cell--content" href="<?= get_the_permalink() ?>">
                <div class="carousel-cell--category">
                    <?php foreach($categories as $category): ?>
                        <span onclick="window.location = '/<?= $category->taxonomy?>/<?= $category->slug ?>'"><?= $category->name ?></span>
                    <?php endforeach; ?>
                </div>
                <h3 class="carousel-cell--title"><?= get_the_title() ?></h3>
            </a>
        </div>
    <?php
    } 

    wp_reset_query();
    ?>

</div>