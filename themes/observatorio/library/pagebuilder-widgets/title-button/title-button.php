<?php

/*
  Widget Name: Título com Botão
  Description: Um título em destaque com um botão call-to-action
  Author: hacklab/
  Author URI: https://hacklab.com.br
 */

namespace widgets;

class TitleButton extends \SiteOrigin_Widget {

    function __construct() {
        $fields = [
            'title' => [
                'type' => 'text',
                'label' => 'Título'
            ],
            'button_text' => [
                'type' => 'text',
                'label' => 'Texto do Botão'
            ],
            'button_url' => [
                'type' => 'text',
                'label' => 'Link do Botão'
            ],
            'button_target' => [
                'type' => 'select',
                'label' => 'Abrir o link em',
                'options' => [ '_self' => 'Na mesma janela', '_blank' => 'Numa nova janela' ]
            ],
        ];

        parent::__construct('title-button', 'Título com Botão', [
            'panels_groups' => [WIDGETGROUP_BANNERS],
            'description' => 'Um título em destaque com um botão call-to-action'
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('title-button', __FILE__, 'widgets\TitleButton');
