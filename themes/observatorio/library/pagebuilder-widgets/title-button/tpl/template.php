<div class="title-button">
    <h3><?= $instance['title']?></h3>
    <a class="button" href="<?= $instance['button_url']?>" target="<?= $instance['button_target']?>"><?= $instance['button_text']?></a>
</div>