<div class="row">

<card-featured-section 
    class="mt-30"
    hash="<?= $hash ?>"
    title="<?= $instance['title'] ?>" 
    :columns="<?= $instance['column'] ?>" >
</card-featured-section>

</div>