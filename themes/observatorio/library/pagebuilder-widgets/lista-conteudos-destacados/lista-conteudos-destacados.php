<?php

/*
  Widget Name: Lista de Conteúdos Destacados
  Description: Adiciona uma lista de conteúdos destacados
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class ListaConteudosDestacados extends \SiteOrigin_Widget
{


    function __construct()
    {

        $fields = [
            'title' => [
                'type' => 'text',
                'label' => 'Título',
            ],
            'column' => [
                'type' => 'number',
                'label' => 'Quantidade de Cards por Linha',
                'default' => '1'
            ],
            'conteudo' => [
                'type' => 'posts',
                'label' => 'Conteúdo'
            ],
        ];

        parent::__construct('lista-conteudos-destacados', "Lista de Conteúdos Destacados", [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Adiciona uma lista de conteúdos destacados'
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance)
    {
        return 'template';
    }

    function get_style_name($instance)
    {
        return 'style';
    }

    function get_template_variables($instance, $args)
    {
        $query_args = siteorigin_widget_post_selector_process_query($instance['conteudo']);
        $taxonomy_term = [];

        $query_args['paged'] = 1;

        $hash = md5(json_encode($query_args));
        set_transient($hash, $query_args);

        return ['hash' => $hash ];
    }

}

Siteorigin_widget_register('lista-conteudos-destacados', __FILE__, 'widgets\ListaConteudosDestacados');
