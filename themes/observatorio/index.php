<?php
get_header();

if(is_author()){
    $curauth = $wp_query->get_queried_object();
    $sidebar_slug = 'autor'; 
} else if(is_tag() || is_category() || is_tax()) {
    $sidebar_slug = 'categoria'; 
} else {
    $sidebar_slug = 'busca'; 
}

$archive = $wp_query->get('archive');
$title = '';
?>
<div class="row mt-20">
    <?php if(is_author()): $title = 'Conteúdo de '.$curauth->display_name ?>
        <div class="column large-12 small-12 text-center author--info">
            <?= get_avatar($curauth->ID) ?>

            <div>
                <h2 class="author--info-name fz-40"><strong><?= $curauth->display_name ?></strong> </h2>
                <div class="author--info-biography"><?= nl2br(get_the_author_meta('description')) ?></div>
            </div>
        </div>
    <?php elseif(is_tag() || is_category() || is_tax() ): ?>
        <div class="column large-12 small-12 text-center">
            <h2 class="mt-30 mb-50 fz-40"><?php echo is_tag() ? "Tag:" : (is_category() ? "Categoria:" : "") ?> <strong><?= single_cat_title( '', false ) ?></strong> </h2>
        </div>
    <?php elseif(is_search()): $title = 'Resultados da Busca' ?>
        <div class="column large-12 small-12 text-center mb-60">
            <h2 class="bolder mt-30 mb-30 fz-40"> Resultado da Busca </h2>
            <h3 class="fz-30 mb-5"><?= $_GET['s'] ?></h3>
            <hr class="small">
            <h5 class="fz-17 mt-20 text-lightgray"><?php global $wp_query; echo $wp_query->post_count ?> resultados encontrados</h5>
        </div>
    <?php endif; ?>

    <?php templates\part('posts-list-with-sidebar', [  'title' => $title, 'slug' => $sidebar_slug ]); ?>
    
</div>

<?php get_footer();
