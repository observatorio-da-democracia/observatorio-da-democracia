import Vue from 'vue';
import store from './store'
import Axios from 'axios'
import CardSection from './components/CardSection';
import CardFeaturedSection from './components/CardFeaturedSection';
import CardList from './components/CardList';

Vue.prototype.$http = Axios;

Vue.component('card-section', CardSection);
Vue.component('card-featured-section', CardFeaturedSection);
Vue.component('card-list', CardList);

const app = new Vue({
    el: '#app',
    store,
    data: {
        assets_dir: window['assets_dir']
    }
})

Array.prototype.first = function () {
    return typeof this[0] != 'undefined' ? this[0] : false;
};

function printElem(elem) {
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write(`
    <html><head>
    <link rel="stylesheet" href="/wp-content/themes/melhoresfilmes/dist/app.css" type="text/css" />
    <link rel="stylesheet" href="/wp-content/plugins/gutenberg/build/block-library/style.css?ver=1540928884" type="text/css" />
    `);
    mywindow.document.write('</head><body >');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    setTimeout(() => {
        mywindow.print();
        mywindow.close();
    }, 500);

    return true;
}

jQuery(document).ready(function () {
    if (jQuery('.perfect-scrollbar-x').length > 0) {
        const scb = new PerfectScrollbar('.perfect-scrollbar-x', {
            useBothWheelAxes: true,
            swipeEasing: true,
            suppressScrollY: true,
        });
    }

    if (jQuery('.perfect-scrollbar').length > 0) {
        const scb = new PerfectScrollbar('.perfect-scrollbar', {
            suppressScrollX: true,
        });
    }

    scrollMenu();

    jQuery('.component-wrapper').each(function() {
        if(jQuery(this).parent().outerWidth() < 480){
            jQuery(this).addClass('small');
        }
    });

    setInterval(function(){
        jQuery('.component-wrapper').each(function() {
            if(jQuery(this).parent().outerWidth() < 480){
                jQuery(this).addClass('small');
            }
        });
    }, 5000)
});

jQuery(window).resize(function () {
    scrollMenu();
});

function scrollMenu() {
    if (window.outerWidth >= 1024) {
        jQuery(window).scroll(function () {
            if (window.scrollY >= 140) {
                jQuery('header').addClass('scrolled');
            } else {
                jQuery('header').removeClass('scrolled');
            }
        });
    }
}

