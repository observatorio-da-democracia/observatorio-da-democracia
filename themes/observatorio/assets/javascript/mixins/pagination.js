export default {
    data() {
        return {
            page: 1,
            totalPages: 3,
            loading : false,
            posts : [],
            featuredPost : false,
            args : {
                'page' : 1,
                'terms' : [],
            }
        }
    },
    methods: {
        loadPosts() {
            this.loading = true;

            this.$http.post(`/wp-json/observatorio/v1/card-section`, { 
                'hash' : this.hash, 
                'args' : this.args, 
                'subhome' : typeof window['subhome'] != 'undefined' ? window['subhome'] : 'false' 
            })
            .then(response => {
                if(!response.data.posts){
                    this.posts = [];
                }else{
                    let posts = response.data.posts;
                    
                    this.totalPages = Math.ceil(response.data.posts_count / 9);
                    
                    if(typeof this.hasLargePost != 'undefined' && this.hasLargePost){
                        this.featuredPost = posts.filter( x => x.featured ).first();
                        this.posts = posts.filter(x => x.ID != this.featuredPost.ID );
                    }else{
                        this.posts = posts;
                    }
                }
                this.loading = false;
            })
        },
        paginationNext() {
            if (this.args.page < (this.totalPages)) {
                this.args.page = parseInt(this.args.page) + 1;
                this.loadPosts();
            }
        },
        paginationPrevious() {
            if (this.args.page - 1 > 0) {
                this.args.page = parseInt(this.args.page) - 1;
                this.loadPosts();
            }
        }
    }
}