<!DOCTYPE html>
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes();?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset');?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1.0">
	<?php wp_head()?>
	<title><?= is_front_page() ? get_bloginfo('name') : wp_title()?></title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="icon" href="<?= get_stylesheet_directory_uri() ?>/assets/images/favicon.ico?v=2" />
</head>
<body <?php body_class();?> >
	<header class="header-menu-mobile show-for-small-only">
		<a href="javascript:void(0);" onclick="jQuery('.header-menu').addClass('active')"><i class="fas fa-bars"></i></a>
		<div class="logo">
			<a href="/"><img src="<?= get_stylesheet_directory_uri() ?>/assets/images/logo.png" alt=""></a>
		</div>
		<a href="javascript:void(0);" onclick="jQuery('.header-menu').addClass('active'); jQuery('.header-menu input').focus()"><i class="fas fa-search"></i></a>
	</header>
	<header class="header-menu">
		<div class="close show-for-small-only" onclick="jQuery('.header-menu').removeClass('active')">&times;</div>

		<div class="top-menu--wrapper">
			<div class="row justify-space-between">
				<div class="column large-6 small-12">
					<?php wp_nav_menu(['theme_location' => 'top-menu', 'container' => 'nav', 'container_class' => 'top-menu' ]) ?>
				</div>

				<div class="column large-6 small-12 right">
					<ul class="social-networks">
						<li><a href="#"><i class="fab fa-youtube"></i></a></li>
						<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#"><i class="fab fa-instagram"></i></a></li>
					</ul>

					<a href="#" class="button small ml-30">Cadastre-se</a>
				</div>
			</div>
		</div>

		<div class="logo hide-for-small-only">
			<a href="/"><img src="<?= get_stylesheet_directory_uri() ?>/assets/images/logo.png" alt=""></a>
		</div>

		<div class="main-menu--wrapper">
			<div class="row">
				<div class="logo-white">
					<img src="<?= get_stylesheet_directory_uri() ?>/assets/images/logo_white.png" alt="">
				</div>
				<div class="static">
					<?php wp_nav_menu(['theme_location' => 'main-menu', 'container' => 'nav', 'container_class' => 'main-menu' ]) ?>
					<form action="/" class="search-form" method="GET">
						<input type="text" name="s" placeholder="buscar">
						<a href="javascript:void(0);" class="hide-for-small-only" onclick="jQuery('.search-form').toggleClass('active')"><i class="fas fa-search"></i></a>
					</form>
				</div>
			</div>
		</div>

	</header>

	<script>
		var assets_dir = '<?= get_stylesheet_directory_uri() ?>/assets';
	</script>

	<div id="app">