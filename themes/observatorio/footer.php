</div>

<footer class="main-footer">
    <div class="row">
        <div class="column large-4">
            <img class="main-footer--logo" src="<?= get_stylesheet_directory_uri() ?>/assets/images/logo_white.png" alt="">
            <?php wp_nav_menu(['theme_location' => 'footer-menu']); ?>
            <h3>Contato:</h3>
            <ul class="menu">
                <li>email@email.com</li>
                <li>+55 11 2847 2749</li>
            </ul>
        </div>
        <div class="column large-3">
            <h3>Editorias:</h3>
            <?=wp_nav_menu(['theme_location' => 'footer-menu--editorias'])?>
            <h3>Siga-nos:</h3>
            <ul class="social-networks">
                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
            </ul>
        </div>
        <div class="column large-5">
            <h3>Cadastre-se:</h3>
            <form class="row cadastre-se" action="">
                <div class="column large-4">
                    <input type="email" name="email" placeholder="Seu email">
                </div>
                <div class="column large-4">
                    <input type="tel" name="phone" placeholder="Seu telefone">
                </div>
                <div class="column large-2">
                    <button type="button">Enviar</button>
                </div>
            </form>
            <h3>Fundações:</h3>
        </div>
    </div>
</footer>
<?php wp_footer() ?>

</body>
</html>