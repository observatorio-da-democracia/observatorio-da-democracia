<?php get_header(); the_post(); ?>

<div class="post-content" id="postContent">
    <div class="row">
        <div id="single-the-title" class="column large-12 small-12 text-center mt-30">
            <h1 class="fz-40"><?php the_title() ?></h1>
        </div>
    </div>
    <div class="row row-small">
        <?php if(has_excerpt()): ?>
        <div id="single-the-excerpt" class="column large-12 small-12 text-center mb-30 ">
            <div class="post-excerpt"><?php the_excerpt() ?></div>
        </div>

        <?php endif ?>
        <div class="column large-12 small-12 mb-30 ">
            <div class="post-info">
                <div class="first-line">
				    <div class="category"><?php the_category(', ') ?></div>
                </div>
				<div class="author">por <?php the_author_posts_link() ?></div>
                <p class="date">
                    Publicado <?php the_date("d/m/Y") ?> às <?php the_time() ?>
                    <?php if(get_the_date() != get_the_modified_date() || get_the_time() != get_the_modified_time()): ?>
                    - Atualizado <?php the_modified_date("d/m/Y") ?> às <?php the_modified_time() ?>
                    <?php endif ?>
                </p>
            </div>
        </div>
        <div id="single-the-content" class="column large-12 small-12">
            <?php the_content(); ?>
        </div>

        <?php if(has_tag()): ?>
        <div class="column large-12 small-12">
            <div class="post-content--tags">
                <div class="post-content--section-title">Tags</div>
                <?php the_tags('') ?>
            </div>
        </div>
        <?php endif; ?>

        <div class="column large-12 small-12">
            <div class="post-content--subshare">
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink() ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                <a href="https://twitter.com/intent/tweet?text=<?= urlencode(get_the_title()) ?>&url=<?= get_the_permalink() ?>" target="_blank"><i class="fab fa-twitter"></i></a>
                <a href="whatsapp://send?text=<?= (get_the_title().' - '.get_the_permalink()) ?>" target="_blank" class="hide-for-large"><i class="fab fa-whatsapp"></i></a>
                <a href="https://api.whatsapp.com/send?text=<?= (get_the_title().' - '.get_the_permalink()) ?>" class="show-for-large" target="_blank"><i class="fab fa-whatsapp"></i></a>
                <a href="https://telegram.me/share/url?url=<?= get_the_title().' - '.get_the_permalink() ?>" target="_blank"><i class="fab fa-telegram"></i></a>
            </div>

            <div class="post-content--author">
                <a href="<?= get_the_author_link() ?>">
                    <b>por </b><h4 class="post-content--author-name"> <?php the_author_posts_link() ?></h4>
                    <div class="post-content--author-biography"><?= nl2br(get_the_author_meta('description')) ?></div>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="column large-12 small-12">
            <div class="post-content--related-posts">
                <?php
                $related_posts_query = get_related_posts();
                $related_posts = [];
                
                if ( $related_posts_query->have_posts() ) : ?>
                        <?php 
                        while( $related_posts_query->have_posts() ) {
                            $related_posts_query->the_post(); 
                            $related_posts[] = prepare_post();
                        } 
                        $widget_id = rand(0, 10000);
                        wp_localize_script('no-js', 'card_list_' . $widget_id, $related_posts);
                        ?>
                        <card-list size="large" :columns="3" title="Veja Também" widget_id="<?= $widget_id ?>"></card-list>
                <?php endif ?>
                <?php wp_reset_postdata(); ?>
            </div>

            <div class="post-content--comments">
                <?php comments_template() ?>
            </div>
        </div>
    </div>

    <div class="post-content--share">
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink() ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
        <a href="https://twitter.com/intent/tweet?text=<?= urlencode(get_the_title()) ?>&url=<?= get_the_permalink() ?>" target="_blank"><i class="fab fa-twitter"></i></a>
        <a href="whatsapp://send?text=<?= (get_the_title().' - '.get_the_permalink()) ?>" target="_blank" class="hide-for-large"><i class="fab fa-whatsapp"></i></a>
        <a href="https://api.whatsapp.com/send?text=<?= (get_the_title().' - '.get_the_permalink()) ?>" class="show-for-large" target="_blank"><i class="fab fa-whatsapp"></i></a>
        <a href="https://telegram.me/share/url?url=<?= get_the_title().' - '.get_the_permalink() ?>" target="_blank"><i class="fab fa-telegram"></i></a>
        <a href="mailto:?subject=<?= the_title() ?>&body=<?= get_the_permalink() ?>" target="_blank"><i class="far fa-envelope"></i></a>
        <a href="javascript:void(0);" onclick="printElem('postContent')" target="_blank"><i class="fas fa-print"></i></a>
    </div>
</div>

<?php get_footer();