<?php
$widget_id = rand(0, 10000);
$posts = [];

while ( have_posts() ){
    the_post();
    $posts[] = prepare_post();
}

wp_localize_script('no-js', 'card_list_' . $widget_id, $posts);
?>

<div class="column large-8 small-12">
    <div class="search-content">
        <card-list title="<?= $title ?>" widget_id="<?= $widget_id ?>"></card-list>
    </div>

    <div class="pagination-numbers">
        <?php pagination(); ?>
    </div>
</div>

<div class="column large-4 small-12 mt-60">
    <?php
        global $post;
        $post = get_page_by_path($slug.'-sidebar', OBJECT, 'layout_parts');
        
        if($post){
            setup_postdata($post);
            the_content();
        }

        wp_reset_postdata();
    ?>
</div>

<div class="column large-12 small-12 mt-60">
<?php
    global $post;
    $post = get_page_by_path($slug.'-footer', OBJECT, 'layout_parts');
    
    if($post){
        setup_postdata($post);
        the_content();
    }

    wp_reset_postdata();
?>
</div>