<?php
$show_category = isset($show_category) && $show_category;
$show_author = isset($show_author) && $show_author;
$show_excerpt = isset($show_excerpt) && $show_excerpt;
$show_channel = isset($show_channel) && $show_channel;

$horizontal = isset($horizontal) && $horizontal;
?>
<a href="<?= get_the_permalink() ?>">
    <div class="card <?php if($horizontal) echo 'horizontal' ?>">
        <?php if($horizontal && (images\tag('card-small', 'card--image') != '') ): ?>
                <div class="card-horizontal--image">
                    <?php echo images\tag('card-small', 'card--image') ?>
                    
                    <?php if($show_channel ):  ?>
                    <div class="card--category">
                        <span class="card--category-title"><?php the_channel() ?></span>
                    </div>
                    <?php endif ?>
                </div>

        <?php else: ?>
            <div class="card--image <?= $horizontal ? 'mt-15' : '' ?>" style="background-image: url('<?php echo images\url('card-medium') ?>')">
                
                <?php if(in_array(get_post_type(), ['podcasts', 'videos']) ): ?>
                    <div class="card--category">
                        <span class="card--category-title"> <i class="text-outrasmidias fas fa-<?= get_post_type() == 'videos' ? 'video' : 'microphone' ?>"></i></span>
                    </div>
                <?php elseif($show_channel): ?>
                    <div class="card--category">
                        <span class="card--category-title"><?php the_channel() ?></span>
                    </div>
                <?php endif ?>
            </div>
        <?php endif ?>

        <a href="<?= get_the_permalink() ?>">
            <h2 class="card--title"><?php the_title() ?></h2>
            <?php if($show_excerpt): ?><div class="card--excerpt"><?php the_excerpt() ?></div><?php endif ?>
        </a>

        <?php if($show_category): ?>
        <div class="card--category">
            <?php if($show_category): ?><span class="card--category-title"><?php the_category(', ') ?></span><?php endif ?>
            <?php if($show_author && $show_category): ?> | <?php endif ?>
            <?php if($show_author): ?> por <?php the_author_posts_link(); ?><?php endif ?>
        </div>
        <?php endif; ?>

        <?php if(!$show_category && $show_author): ?>
            <div class="card--author">por <?php the_author_posts_link(); ?></div>
        <?php endif; ?>
    </div>
</a>    
