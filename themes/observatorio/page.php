<?php get_header(); ?>

<div class="page-content" id="pageContent">
    <?php the_post(); ?>

    <div class="row row-small">
        <div class="column large-12 small-12 text-center mt-40 mb-30">
            <h1><strong><?php the_title() ?></strong></h1>
        </div>
        <div class="column large-12 small-12 mb-30">
            <?php the_content(); ?>
        </div>
    </div>
</div>

<?php get_footer();