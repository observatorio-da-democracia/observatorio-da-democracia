<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>404 Página Não encontrada</title>
</head>
<body class="error-404">
    <div>
        <h1> Erro <br> <strong>404</strong> </h1>
        <p>Página não encontrada</p>
    </div>

    <a href="/" class="button"> <span>Voltar para a página inicial</span> </a>
</body>
</html>